<?php
/**
 * FoodCare Extension for Yii
 * Developed by: me
 */

class FoodCare extends CApplicationComponent
{
    public $token;

    public $foodCareUrl;
    public $searchUrl;
    public $showUrl;

    public function init()
    {
        if(empty($this->foodCareUrl))
            $this->foodCareUrl = 'http://api.foodcare.me/';
        if(empty($this->searchUrl))
        	$this->searchUrl='foodstuffs/list';
        if(empty($this->showUrl))
        	$this->showUrl='foodstuffs/show';
    }

    public function search($term,$page=1,$per_page=10)
    {
        $data=array();
        $index=0;
    	$results=$this->curlWrap($this->searchUrl.'/facts?q='.$term.
        					   '&page='.$page.'&per_page='.$per_page);
        foreach ($results as $result)
        foreach ($result as $item)
        if (isset($item['name']) && $item['brand_name']===null)
        	$data[]=array(
        			'id'=>$index++,
        			'value'=>$item['name'],
        			'ingredientId'=>$item['id'], 
        	);
        return $data;
    }

    /**
     * 
     * @param unknown $url
     * @param string $json
     * @param string $action
     * @return Associative array
     */
    public function curlWrap($url, $json=null, $action='GET')
    {
    	$ch = curl_init();
    	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    	curl_setopt($ch, CURLOPT_MAXREDIRS, 10 );
    	curl_setopt($ch, CURLOPT_URL, $this->foodCareUrl.$url);
    	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'X-Access-Token: h3sn2gsa7h6wqkmgekbwf5qg',
            'Accept: application/json',
        ));
    	curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    	curl_setopt($ch, CURLOPT_VERBOSE, true);
    	switch($action){
    		case "POST":
    			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    			break;
    		case "GET":
    			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    			break;
    		case "PUT":
    			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    			break;
    		case "DELETE":
    			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    			break;
    		default:
    			break;
    	}

    	$output = curl_exec($ch);
    	curl_close($ch);
    	return CJSON::decode($output,true);
    }

}
