<?php
/**
 * Nutritionix Extension for Yii
 * Developed by: me
 * 
 * https://api.nutritionix.com/v1_1/search/mcdonalds?results=0:20&fields=item_name,brand_name,item_id,nf_calories&appId=APPID&appKey=APPKEY
 */

class Nutritionix extends CApplicationComponent
{
    public $appKey;
    public $appId;
    
    public $url;
    
    public function init()
    {
        if(empty($this->url))
            $this->url = 'https://api.nutritionix.com/v1_1/';
    }

    public function search($term,$page=1,$per_page=10)
    {
    	$data=array();
    	$index=0;
    	$results=$this->curlWrap($this->url.'search/'.$term.'?results='.($page-1)*$per_page.':'.$page*$per_page.
        					   	 '&fields=item_name,brand_name,item_id,nf_calories'.
        						 '&appId='.$this->appId.'&appKey='.$this->appKey);
        foreach ($results['hits'] as $result)
        	$data[]=array(
        		'id'=>$index++,
        		'value'=>$result['fields']['item_name'],
        		'ingredientId'=>$result['_id'],
        	);
        return $data;
    }
    
    public function detail($itemId=null,$upc=null)
    {
    	($itemId=null) ? $searchStr='item?upc='.$upc : $searchStr='item?id='.$itemId;
    	$results=$this->curlWrap($this->url.$searchStr.
    							 '&appId='.$this->appId.'&appKey='.$this->appKey);
    	return $results;
    }

    /**
     * 
     * @param unknown $url
     * @param string $json
     * @param string $action
     * @return Associative array
     */
    public function curlWrap($url, $json=null, $action='GET')
    {
    	$ch = curl_init();
    	curl_setopt($ch, CURLOPT_URL, $url);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	switch($action){
    		case "POST":
    			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    			break;
    		case "GET":
    			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    			break;
    		case "PUT":
    			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    			break;
    		case "DELETE":
    			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    			break;
    		default:
    			break;
    	}

    	$output = curl_exec($ch);
    	curl_close($ch);
    	return CJSON::decode($output,true);
    }

}
