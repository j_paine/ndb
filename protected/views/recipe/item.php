<?php
/* @var $this RecipeController */
/* @var $model Recipe */
/* @var $item Item */

$this->breadcrumbs=array(
		'Recipes'=>array('index'),
		$model->name=>array('recipe/'.$model->id),
		'Edit Items',
);

$this->menu=array(
		array('label'=>'List Recipe', 'url'=>array('index')),
		array('label'=>'Manage Recipe', 'url'=>array('admin')),
		array('label'=>'Add Ingredients', 'url'=>array('//ingredient/create')),
);
?>

<?php $this->renderPartial('//item/_form', array(
	'model'=>$model,
	'item'=>$item,
)); ?>