<?php
/* @var $this MealController/RecipeController/ActivityController */
/* @var $model Meal/Recipe/Activity */
/* @var $item ItemForm */
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'item-grid',
		'dataProvider'=>$model->getItems(),
		//'filter'=>$item,
		'columns'=>array(
				'id',
				'name',
				'amount',
				'unit',
				'type',
				array(
						'class'=>'CButtonColumn',
						'template'=>'{view} {update} {delete}',
						'buttons'=>array(
							'view'=>array(
								'label'=>'View Item',
								'url'=>'Yii::app()->createUrl($data["type"],array("view"=>$data["itemId"]))',
							),
							'update'=>array(
								'label'=>'Edit Item',
								'url'=>'Yii::app()->createUrl($data["type"],array("update"=>$data["itemId"]))',
							),
							'delete'=>array(
								'label'=>'Delete Item',
								'url'=>'Yii::app()->createUrl($data["parentType"].
									"/deleteItem",array("type"=>$data["type"],"itemId"=>$data["itemId"],"parentId"=>$data["parentId"])
								)',
							),
						),
				),
		),
)); ?>
