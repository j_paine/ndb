<?php
/* @var $this MealController/RecipeController/ActivityController */
/* @var $model Meal/Recipe/Activity */
/* @var $item ItemForm */
/* @var $form CActiveForm */
?>

<h1>Add Items to <?php echo $model->name; ?></h1>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'item-form',
	'enableAjaxValidation'=>true,
)); ?>

	<?php echo $form->errorSummary($item); ?>
	<?php echo $form->hiddenField($item,'type'); ?>
	<?php echo $form->hiddenField($item,'itemId'); ?>
	
	<div class="row">
		<?php echo $form->labelEx($item,'name'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
			'name'=>'name',
			'sourceUrl'=>Yii::app()->createUrl('meal/ajaxGetItems'),
			'options'=>array(
				'minLength'=>1,
				'type'=>'get',
				'select'=>'js:function(event,ui) {
						$("#'.CHtml::activeId($item, "type").'").val(ui.item.type);
						$("#'.CHtml::activeId($item, "itemId").'").val(ui.item.id);
						$("#unit_span").text(ui.item.unit);
					}',
				'change'=>'js:function(event,ui) {
						if (!ui.item) {
							$("#'.CHtml::activeId($item, "type").'").val("");
							$("#'.CHtml::activeId($item, "itemId").'").val("");
							$("#unit_span").text("");
						}
					}',
			),
		)); ?> 
		<?php echo $form->error($item,'name'); ?>
		<?php echo $form->error($item,'type'); ?>
 	</div>
	
	<div class="row">
		<?php echo $form->labelEx($item,'amount'); ?>
		<?php echo $form->textField($item,'amount',array('size'=>5,'maxlength'=>5)); ?>
		<span id="unit_span">unit</span>
		<?php echo $form->error($item,'amount'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton('Add Item'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php $this->renderPartial('//item/_gridItems',array(
		'model'=>$model,
		'item'=>$item,
)); ?>



