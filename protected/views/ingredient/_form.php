<?php
/* @var $this IngredientController */
/* @var $model Ingredient */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ingredient-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php //echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100)); ?>
		<?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
			'name'=>'name',
			'sourceUrl'=>Yii::app()->createUrl('ingredient/ajaxSearchIngredients'),
			'options'=>array(
				'minLength'=>2,
				'type'=>'get',
				'select'=>'js:function(event,ui) {
								$.ajax({
									url: '.Yii::app()->createUrl("ingredient/ajaxGetIngredient/".'ui.item.ingredientId'.")',
									type: "POST",
									
								})'
					
					//$("#'.CHtml::activeId($item, "type").'").val(ui.item.type);
					//			$("#'.CHtml::activeId($item, "itemId").'").val(ui.item.id);
					//			$("#unit_span").text(ui.item.unit);
					//		}',
			),
		)); ?> 
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'unit'); ?>
		<?php echo $form->textField($model,'unit',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'unit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'calories'); ?>
		<?php echo $form->textField($model,'calories'); ?>
		<?php echo $form->error($model,'calories'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->