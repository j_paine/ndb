<?php
/* @var $this MealController */
/* @var $model Meal */
/* @var $item Item */

$this->breadcrumbs=array(
		'Meals'=>array('index'),
		$model->name=>array('meal/'.$model->id),
		'Edit Items',
);

$this->menu=array(
		array('label'=>'List Meal', 'url'=>array('index')),
		array('label'=>'Manage Meal', 'url'=>array('admin')),
		array('label'=>'Add Ingredients', 'url'=>array('//ingredient/create')),
		array('label'=>'Add Recipes', 'url'=>array('//recipe/create')),
);
?>

<?php $this->renderPartial('//item/_form', array(
	'model'=>$model,
	'item'=>$item,
)); ?>