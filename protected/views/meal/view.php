<?php
/* @var $this MealController */
/* @var $model Meal */
/* @var $item Item */

$this->breadcrumbs=array(
	'Meals'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Meal', 'url'=>array('index')),
	array('label'=>'Create Meal', 'url'=>array('create')),
	array('label'=>'Edit Items', 'url'=>array('items', 'id'=>$model->id)),
	array('label'=>'Delete Meal', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Meal', 'url'=>array('admin')),
);
?>

<h1>View Meal #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'user_id',
		'name',
		'time',
		'id',
	),
)); ?>

<?php $this->renderPartial('//item/_gridItems',array(
	'model'=>$model,
	'item'=>$item,
)); ?>