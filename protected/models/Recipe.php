<?php

/**
 * This is the model class for table "{{recipe}}".
 *
 * The followings are the available columns in table '{{recipe}}':
 * @property string $user_id
 * @property string $name
 * @property string $id
 *
 * The followings are the available model relations:
 * @property Meal[] $tblMeals
 * @property User $user
 * @property Ingredient[] $tblIngredients
 */
class Recipe extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{recipe}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('name', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_id, name, id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'meals' => array(self::MANY_MANY, 'Meal', '{{meal_has_recipe}}(recipe_id, meal_id)'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'hasIngredients' => array(self::HAS_MANY, 'RecipeHasIngredient', 'recipe_id', 'with'=>'ingredient'),
			'ingredients' => array(self::HAS_MANY, 'Ingredient', 'ingredient_id', 'through'=>'hasIngredients'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'name' => 'Name',
			'id' => 'ID',
		);
	}

	public function beforeSave()
	{
		$this->user_id=Yii::app()->user->id;
		return parent::beforeSave();
		
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('id',$this->id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getItems()
	{
		$index=0;
		$data=array();
		foreach($this->hasIngredients as $item)
		{
			$index++;
			$data[]=array(
					'id'=>$index,
					'name'=>$item->ingredient->name,
					'amount'=>$item->amount,
					'unit'=>$item->ingredient->unit,
					'type'=>'ingredient',
					'itemId'=>$item->ingredient->id,
					'parentId'=>$this->id,
					'parentType'=>'Recipe',
			);
		}
		
		return new CArrayDataProvider($data);
	}

	public function getItemViewUrl($itemData)
	{
		if($itemData->type==='ingredient')
			$item=Ingredient::model()->findByAttributes(array('name'=>$itemData->name));
		if($item===null) throw new CDbException("Invalid item");
		return Yii::app()->createUrl($itemData->type.'/view/',array('id'=>$item->id));
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Recipe the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
