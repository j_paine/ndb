<?php

/**
 * This is the model class for table "{{user}}".
 *
 * The followings are the available columns in table '{{user}}':
 * @property string $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property integer $type
 * @property string $updated
 * @property string $created
 *
 * The followings are the available model relations:
 * @property Activity[] $activities
 * @property Exercise[] $exercises
 * @property Ingredient[] $ingredients
 * @property Meal[] $meals
 * @property Measurement[] $measurements
 * @property Recipe[] $recipes
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, email, password', 'required'),
			array('username, email', 'unique'),
			array('created', 'default', 'value'=>new CDbExpression("NOW()")),
			array('type', 'numerical', 'integerOnly'=>true),
			array('type', 'default', 'value'=>0),
			array('username', 'length', 'max'=>25),
			array('email', 'length', 'max'=>255),
			array('password', 'length', 'max'=>64),
			array('updated', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, email, password, type, updated, created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'activities' => array(self::HAS_MANY, 'Activity', 'user_id'),
			'exercises' => array(self::HAS_MANY, 'Exercise', 'user_id'),
			'ingredients' => array(self::HAS_MANY, 'Ingredient', 'user_id'),
			'meals' => array(self::HAS_MANY, 'Meal', 'user_id'),
			'measurements' => array(self::HAS_MANY, 'Measurement', 'user_id'),
			'recipes' => array(self::HAS_MANY, 'Recipe', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'email' => 'Email',
			'password' => 'Password',
			'type' => 'Type',
			'updated' => 'Updated',
			'created' => 'Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('created',$this->created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function validatePassword($pass)
	{
		return CPasswordHelper::verifyPassword($pass, $this->password);
	}
	
	public function hashPassword($pass)
	{
		return CPasswordHelper::hashPassword($pass);
	}
	
	protected function generateSalt()
	{
		return uniqid('',true);
	}
}
