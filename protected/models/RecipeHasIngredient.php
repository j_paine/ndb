<?php

/**
 * This is the model class for table "{{recipe_has_ingredient}}".
 *
 * The followings are the available columns in table '{{recipe_has_ingredient}}':
 * @property string $recipe_id
 * @property string $ingredient_id
 * @property double $amount
 */
class RecipeHasIngredient extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{recipe_has_ingredient}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('recipe_id, ingredient_id, amount', 'required'),
			array('amount', 'numerical'),
			array('recipe_id, ingredient_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('recipe_id, ingredient_id, amount', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'ingredient'=>array(self::HAS_ONE, 'Ingredient', array('id'=>'ingredient_id')),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'recipe_id' => 'Recipe',
			'ingredient_id' => 'Ingredient',
			'amount' => 'Amount',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('recipe_id',$this->recipe_id,true);
		$criteria->compare('ingredient_id',$this->ingredient_id,true);
		$criteria->compare('amount',$this->amount);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RecipeHasIngredient the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
