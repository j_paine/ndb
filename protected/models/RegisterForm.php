<?php

class RegisterForm extends CFormModel
{
	public $username;
	public $email;
	public $password;
	public $verifyCode;

	private $_identity;
	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
				// username, email, password are required
				array('username, email, password', 'required'),
				array('username', 'isunique'),
				// email has to be a valid email address
				array('email', 'email'),
				// verifyCode needs to be entered correctly
				array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
				'verifyCode'=>'Verification Code',
		);
	}
	
	public function isunique()
	{
		if(!$this->hasErrors())
			if(User::model()->find("LOWER('username')=?",array($this->username))!==null)
				$this->addError('username','Username taken');
	}
	
	public function register()
	{
		if(!$this->hasErrors())
		{
			$this->_identity=new UserIdentity($this->username,
								$this->password,$this->email);
			if(!$this->_identity->create())
			{
				$this->addError('password','Registration failed');
				return false;
			} else {
				Yii::app()->user->login($this->_identity,0);
				return true;
			}
			
		}
	}
}