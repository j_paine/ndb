<?php
class ItemForm extends CFormModel
{
	public $name;
	public $amount;
	public $itemId;
	public $unit;
	
	public $parent;
	public $type;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
				array('name, amount', 'required'),
				array('type', 'in', 'range'=>array('Ingredient','Recipe','Exercise'), 
						'allowEmpty'=>false, 'message'=>'Please enter a valid item'),			
				array('amount', 'numerical', 'integerOnly'=>false),
				array('itemId', 'safe'),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
				
		);
	}
	
	
	public function saveItem($id)
	{
		$modelName=$this->parent.'Has'.$this->type;
		$parentId=lcfirst($this->parent).'_id';
		$childId=lcfirst($this->type).'_id';
		
		$assocModel=new $modelName;
		$assocModel->amount=$this->amount;
		$assocModel->$parentId=$id;
		$assocModel->$childId=$this->itemId;
		
		if(!$assocModel->validate()) throw new CDbException("Item validation failed");
		if(!$assocModel->save()) throw new CDbException("Saving item failed");
	}
}