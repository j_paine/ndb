<?php

/**
 * This is the model class for table "{{meal}}".
 *
 * The followings are the available columns in table '{{meal}}':
 * @property string $user_id
 * @property string $name
 * @property string $time
 * @property string $id
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Ingredient[] $tblIngredients
 * @property Recipe[] $tblRecipes
 */
class Meal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{meal}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_id, name, time, id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'hasIngredients' => array(self::HAS_MANY, 'MealHasIngredient', 'meal_id', 
					'with'=>'ingredient', 'together'=>true),
			'hasRecipes' => array(self::HAS_MANY, 'MealHasRecipe', 'meal_id',
					'with'=>'recipe', 'together'=>true),
			'ingredients' => array(self::HAS_MANY, 'Ingredient', 'ingredient_id', 'through'=>'hasIngredients'),
			'recipes' => array(self::HAS_MANY, 'Recipe', 'recipe_id', 'through'=>'hasRecipes'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'name' => 'Name',
			'time' => 'Time',
			'id' => 'ID',
		);
	}
	
	public function beforeSave()
	{
		$this->user_id=Yii::app()->user->id;
		$this->time=new CDbExpression("NOW()");
		return parent::beforeSave();
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
	
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('time',$this->time,true);
		$criteria->compare('id',$this->id,true);
	
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getItems()
	{
		$index=0;
		$data=array();
		foreach($this->hasIngredients as $item)
		{
			$index++;
			$data[]=array(
					'id'=>$index,
					'name'=>$item->ingredient->name,
					'amount'=>$item->amount,
					'unit'=>$item->ingredient->unit,
					'type'=>'ingredient',
					'itemId'=>$item->ingredient->id,
					'parentId'=>$this->id,
					'parentType'=>'Meal',
			);
		}
		foreach($this->hasRecipes as $item)
		{
			$index++;
			$data[]=array(
					'id'=>$index,
					'name'=>$item->recipe->name,
					'amount'=>$item->amount,
					'unit'=>null,
					'type'=>'recipe',
					'itemId'=>$item->recipe->id,
					'parentId'=>$this->id,
					'parentType'=>'Meal',
			);
		}
		return new CArrayDataProvider($data);
	}
	
	public function getItemViewUrl($itemData)
	{
		if($itemData->type==='ingredient')
			$item=Ingredient::model()->findByAttributes(array('name'=>$itemData->name));
		else if($itemData->type==='recipe')
			$item=Recipe::model()->findByAttributes(array('name'=>$itemName));
		if($item===null) throw new CDbException("Invalid item");
		return Yii::app()->createUrl($itemData->type.'/view/',array('id'=>$item->id));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Meal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
