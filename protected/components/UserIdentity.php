<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	public $email;
	private $_id;
	
	public function __construct($username, $password, $email=null)
	{
		if($email!==null) $this->email=$email;
		return parent::__construct($username, $password);
	}
	
	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$username=strtolower($this->username);
		$user=User::model()->findByAttributes(array('username'=>$this->username));
		if($user===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else if(!$user->validatePassword($this->password))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
		{
			$this->_id=$user->id;
			$this->username=$user->username;
			$this->errorCode=self::ERROR_NONE;
		}
		return $this->errorCode==self::ERROR_NONE;
	}
	
	/**
	 * Creates a user.
	 * @return boolean whether creation successful
	 */
	public function create()
	{
		$username=strtolower($this->username);
		if(User::model()->find("LOWER('username')=?",array($username)))
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else
		{
			$user=new User;
			$user->username=$this->username;
			$user->email=$this->email;
			$user->password=$user->hashPassword($this->password);
			$user->created=new CDbExpression("NOW()");
			if($user->save()) $this->errorCode=self::ERROR_NONE;
			else $this->errorCode=self::ERROR_PASSWORD_INVALID;
		}
		return $this->errorCode==self::ERROR_NONE;
	}
	
	public function getId()
	{
		return $this->_id;
	}
}