CREATE TABLE IF NOT EXISTS ndb.tbl_user (
id INTEGER UNSIGNED AUTO_INCREMENT,
username VARCHAR(25) NOT NULL,
email VARCHAR(255) NOT NULL,
password VARCHAR(64) NOT NULL,
type TINYINT(1) NOT NULL DEFAULT 0,
updated DATETIME,
created DATETIME NOT NULL,
PRIMARY KEY(id),
UNIQUE INDEX username_UNIQUE (username),
UNIQUE INDEX email_UNIQUE (email)
);

CREATE TABLE IF NOT EXISTS ndb.tbl_meal (
user_id INTEGER UNSIGNED NOT NULL,
name VARCHAR(100),
time DATETIME NOT NULL,
id INTEGER UNSIGNED AUTO_INCREMENT,
PRIMARY KEY(id),
INDEX fk_meal_user_idx (user_id),
CONSTRAINT fk_meal_user
FOREIGN KEY(user_id) REFERENCES tbl_user(id)
ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS ndb.tbl_recipe (
user_id INTEGER UNSIGNED NOT NULL,
name VARCHAR(100) NOT NULL,
id INTEGER UNSIGNED AUTO_INCREMENT,
PRIMARY KEY(id),
INDEX fk_recipe_user_idx (user_id),
CONSTRAINT fk_recipe_user
FOREIGN KEY(user_id) REFERENCES tbl_user(id)
ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS ndb.tbl_ingredient (
user_id INTEGER UNSIGNED NOT NULL,
name VARCHAR(100) NOT NULL,
unit INTEGER UNSIGNED NOT NULL,
calories FLOAT NOT NULL,
id INTEGER UNSIGNED AUTO_INCREMENT,
PRIMARY KEY(id),
INDEX fk_ingredient_user_idx (user_id),
CONSTRAINT fk_ingredient_user
FOREIGN KEY(user_id) REFERENCES tbl_user(id)
ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS ndb.tbl_recipe_has_ingredient (
recipe_id INTEGER UNSIGNED NOT NULL,
ingredient_id INTEGER UNSIGNED NOT NULL,
amount FLOAT NOT NULL,
PRIMARY KEY(recipe_id,ingredient_id),
INDEX fk_recipe_has_ingredient_recipe_idx (recipe_id),
INDEX fk_recipe_has_ingredient_ingredient_idx (ingredient_id),
CONSTRAINT fk_recipe_has_ingredient_recipe
FOREIGN KEY(recipe_id) REFERENCES tbl_recipe(id)
ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT fk_recipe_has_ingredient_ingredient
FOREIGN KEY(ingredient_id) REFERENCES tbl_ingredient(id)
ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS ndb.tbl_meal_has_ingredient (
meal_id INTEGER UNSIGNED NOT NULL,
ingredient_id INTEGER UNSIGNED NOT NULL,
amount FLOAT NOT NULL,
PRIMARY KEY(meal_id,ingredient_id),
INDEX fk_meal_has_ingredient_meal_idx (meal_id),
INDEX fk_meal_has_ingredient_ingredient_idx (ingredient_id),
CONSTRAINT fk_meal_has_ingredient_meal
FOREIGN KEY(meal_id) REFERENCES tbl_meal(id)
ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT fk_meal_has_ingredient_ingredient
FOREIGN KEY(ingredient_id) REFERENCES tbl_ingredient(id)
ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS ndb.tbl_meal_has_recipe (
meal_id INTEGER UNSIGNED NOT NULL,
recipe_id INTEGER UNSIGNED NOT NULL,
amount FLOAT NOT NULL,
PRIMARY KEY(meal_id,recipe_id),
INDEX fk_meal_has_recipe_meal_idx (meal_id),
INDEX fk_meal_has_recipe_recipe_idx (recipe_id),
CONSTRAINT fk_meal_has_recipe_meal
FOREIGN KEY(meal_id) REFERENCES tbl_meal(id)
ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT fk_meal_has_recipe_recipe
FOREIGN KEY(recipe_id) REFERENCES tbl_recipe(id)
ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS ndb.tbl_measurement (
id INTEGER UNSIGNED AUTO_INCREMENT,
time DATETIME NOT NULL,
user_id INTEGER UNSIGNED NOT NULL,
weight FLOAT,
height FLOAT,
goal_cals FLOAT,
PRIMARY KEY(id),
INDEX fk_measurement_user_idx (user_id),
CONSTRAINT fk_measurement_user
FOREIGN KEY(user_id) REFERENCES tbl_user(id)
ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS ndb.tbl_activity (
id INTEGER UNSIGNED AUTO_INCREMENT,
time DATETIME NOT NULL,
user_id INTEGER UNSIGNED NOT NULL,
PRIMARY KEY(id),
INDEX fk_activity_user_idx (user_id),
CONSTRAINT fk_activity_user
FOREIGN KEY(user_id) REFERENCES tbl_user(id)
ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS ndb.tbl_exercise (
user_id INTEGER UNSIGNED NOT NULL,
name VARCHAR(100) NOT NULL,
id INTEGER UNSIGNED AUTO_INCREMENT,
PRIMARY KEY(id),
INDEX fk_exercise_user_idx (user_id),
CONSTRAINT fk_exercise_user
FOREIGN KEY(user_id) REFERENCES tbl_user(id)
ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS ndb.tbl_activity_has_exercise (
activity_id INTEGER UNSIGNED NOT NULL,
exercise_id INTEGER UNSIGNED NOT NULL,
amount FLOAT NOT NULL,
PRIMARY KEY(activity_id,exercise_id),
INDEX fk_activity_has_exercise_exercise_idx (exercise_id),
INDEX fk_activity_has_exercise_activity_idx (activity_id),
CONSTRAINT fk_activity_has_exercise_activity
FOREIGN KEY(activity_id) REFERENCES tbl_activity(id)
ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT fk_activity_has_exercise_exercise
FOREIGN KEY(exercise_id) REFERENCES tbl_exercise(id)
ON DELETE CASCADE ON UPDATE CASCADE
);
